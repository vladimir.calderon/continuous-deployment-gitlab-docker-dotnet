# Continuous Deployment with GitLab, Docker and DotNet

## Overview
Sample created for [DDD Malaga](https://www.meetup.com/DDD-Malaga)'s [event](https://www.meetup.com/DDD-Malaga/events/270105383/) hosted by Diego Martin.

Its goal is to show how a DotNet Core 3.1 web application project can be automated for continuous integration, delivery and deployment.

## Pre-requirements for the workshop
- Linux console (also possible with MacOs or Windows)
- [DotNet Core SDK 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1) installed
- [Azure Command Line Interface](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) installed
- Free account in [Gitlab](https://gitlab.com) created
- Free account in [Azure](https://portal.azure.com) created
- [Docker](https://docs.docker.com/get-docker/) is optional for this workshop but recommended to test the built docker image by running a container locally.

## Create sample DotNetCore 3.1 web application
These are the steps taken to create a project like the current one with a linux console terminal.

Create or clone an empty repository. For example to clone this git repository:
```
git clone https://gitlab.com/ddd-malaga/continuous-deployment-gitlab-docker-dotnet.git
```

Go inside the project folder
```
cd continuous-deployment-gitlab-docker-dotnet
```

Create a new folder for the program source code
```
mkdir src
```

Create a new folder for the program automated tests
```
mkdir test
```

Go inside the `src` folder
```
mkdir src
```

Create a new DotNetCore empty web project with the dotnet CLI
```
dotnet new web -n SampleWebApp
```

Go inside the `test` folder
```
cd ../test
```

Create a new xUnit project with the dotnet CLI
```
dotnet new xunit -n SampleWebApp.UnitTests
```

Go back to main folder
```
cd ..
```

Create a new solution file
```
dotnet new sln -n ContinuousDeploymentGitlabDockerDotnet
```

Add the previous projects to the solution
```
dotnet sln ContinuousDeploymentGitlabDockerDotnet.sln add src/SampleWebApp/ test/SampleWebApp.UnitTests
```

Make sure the solution compiles
```
dotnet build
```

And make sure the web application runs and can be accessed in http://localhost:5000
```
dotnet run -p src/SampleWebApp/
```
Stop the kestrel server with `CTRL+C`.

Create a `.gitlab-ci.yml` empty file at the root project where the CI/CD pipeline will be configured for GitLab to run it automatically.
```
touch .gitlab-ci.yml
```

Optionally add a `.gitignore` file to control which folders and files should not be added to the Git repository (i.e: the ones that are auto-generated).

Add the stages and jobs to `.gitlab-ci.yml` to build the code, run automated tests and build a docker image to store at the container registry. See the final [.gitlab-ci.yml](.gitlab-ci.yml)

Finally, make the necessary changes, add them to git with 
```
git add .
```
Commit them 
```
git commit -m "commit message here"
``` 
and push them to GitLab to trigger the automated CI/CD pipeline that will ensure the code can be continuously integrated and delivered as a docker image.
```
git push
``` 

## Create a personal access token in GitLab
Sometimes we may require ourselves or external applications to access GitLab API functionality on our behalf.

GitLab provides an easy way to [create personal access tokens](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) tied to specific permissions (read only, write, etc.). These tokens should be secret only known by ourselves because they are like passwords that identify our GitLab account.

For this workshop we have created a temporary access token with permissions to access API for read-only operations on my behalf as shown at the image below.

The read-only token generated is: `1jDiVYigZS19znjpgqXM`

![Create personal access token](sample_access_token.png)

## How to run container locally (Optional)
To try out the generated docker image and run it locally as a docker container we can access the GitLab container registry.

Login into the container registry
```
docker login registry.gitlab.com
```
and enter username and password (or the personal access token).

Then run a container, map its port `80` to the `localhost:8080` and optionally assign a name to the container.
```
docker run --name dddmalagasample -p 8080:80 registry.gitlab.com/ddd-malaga/continuous-deployment-gitlab-docker-dotnet:latest
```

The container can be started using its alias with the command `docker start dddmalagasample` and stopped with the command `docker stop dddmalagasample`

Also, don't forget to logout from the gitlab registry by running
```
docker logout registry.gitlab.com
```

## Instructions for CI/CD with Azure Web App
Login in Azure with CLI
```
az login
```

Create resource group under a specific subscription
```
az group create --location westeurope --name resource_group_name --subscription your_subscription_name
```

Create service plan for linux free F1 (there are others more sophisticated)
```
az appservice plan create --name service_plan_name --resource-group resource_group_name --is-linux --sku F1 --location westeurope
```

Create web app for container providing the new web app name, the resource group name, the service plan name, the docker image (with or without latest tag) and the credentials to access Gitlab (username and the personal access token created previously)
```
az webapp create --name webapp_name --resource-group resource_group_name --plan service_plan_name --deployment-container-image-name registry.gitlab.com/ddd-malaga/continuous-deployment-gitlab-docker-dotnet --docker-registry-server-user diegosasw --docker-registry-server-password 1jDiVYigZS19znjpgqXM
```

Enable continuous deployment. A webhook will tell Azure to pull the new image when something changes in GitLab container registry.
```
az webapp deployment container config --name webapp_name --resource-group resource_group_name --enable-cd true 
```

The web applications should be available shortly.
```
curl http://<webapp_name>.azurewebsites.net
```

When new changes are made to the code and pushed to GitLab's git repository, the whole process will trigger automatically compiling, running automated tests and generating a new docker image.

To notify Azure that a new image is ready, we can invoke the webhook that Web App provides with a POST request.
```
curl -X POST https://\$user:password_here@webapp_name.scm.azurewebsites.net/docker/hook -d ""

```