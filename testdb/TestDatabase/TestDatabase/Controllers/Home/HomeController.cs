﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;
using TestDatabase.ViewModels.Home;

namespace TestDatabase.Controllers.Home
{
    public class HomeController : Controller
    {
        private IConfiguration _configuration;

        public HomeController(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ConnectionViewModel model = new ConnectionViewModel();
            model.ConnectionStringFromConfig = _configuration.GetConnectionString("DBConnectionString");
            model.ConnectionString = model.ConnectionStringFromConfig;

            return View(model);
        }

        [HttpPost]
        public IActionResult Index(ConnectionViewModel model)
        {
            model.ConnectionStringFromConfig = _configuration.GetConnectionString("DBConnectionString");

            try
            {
                using (SqlConnection conn = new SqlConnection(model.ConnectionString))
                {
                    string spName = "select 4 as [id] ,'Alpha' as [name]";

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(spName, conn);

                    //open connection
                    conn.Open();

                    //set the SqlCommand type to stored procedure and execute
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    ResultRowViewModel rs = new ResultRowViewModel();
                    //check if there are records
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ProcessRow(dr, rs);
                        }
                    }

                    model.ResultValue = rs;

                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }

                model.ErrorMessage = "OK";
            }
            catch (Exception ex)
            {
                model.ErrorMessage = ex.Message;
            }

            return View(model);
        }

        private void ProcessRow(SqlDataReader dr, ResultRowViewModel rs)
        {
            int idColumnIndex = -1;
            int nameColumnIndex = -1;

            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).ToLower() == "id")
                    idColumnIndex = i;
                if (dr.GetName(i).ToLower() == "name")
                    nameColumnIndex = i;
            }

            int idValue = dr.GetInt32(idColumnIndex);
            string nameValue = dr.GetString(nameColumnIndex);

            rs.Id = idValue;
            rs.Name = nameValue;
        }
    }
}
