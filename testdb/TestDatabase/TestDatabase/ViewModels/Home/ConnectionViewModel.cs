﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestDatabase.ViewModels.Home
{
    public class ConnectionViewModel 
    {
        public string ConnectionStringFromConfig { get; set; }
        public string ConnectionString { get; set; }
        public ResultRowViewModel ResultValue { get; set; }
        public string ErrorMessage { get; set; }
    }
}
