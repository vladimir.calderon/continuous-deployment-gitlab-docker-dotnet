﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestDatabase.ViewModels.Home
{
    public class ResultRowViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
